# F5PoolDecoder

Tool to decode F5 Pool Decoder


## How to install?

Run

```sh
go get gitlab.com/ajabep/F5PoolDecoder
```

It will put the binary into `$GOBIN` directory. Thus, to invoke the `F5PoolDecoder` if `$GOBIN` is not in your `$PATH`,
do:

```sh
`go env GOBIN`/F5PoolDecoder
```


## External Ressource

* F5 documentation about these cookies: https://support.f5.com/csp/article/K6917


## Why?

It exists probably a lot of project doing the same. However, I wanted a simple project to begin to learn golang. That's
done :)
