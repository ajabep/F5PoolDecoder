package main

import (
	"net"
	"strings"
	"testing"
)

func resolveTCPAddr(addrStr string) (*TCPIfAddr, error) {
	ifStr := ""
	if strings.Contains(addrStr, "%") {
		ifSuffix := strings.SplitN(
			addrStr,
			"%",
			2)[1]
		newAddrStr := strings.SplitN(
			addrStr,
			"%",
			2)[0]

		if strings.Contains(ifSuffix, ":") {
			ifStr = strings.SplitN(
				ifSuffix,
				":",
				2)[0]
			newAddrStr += ":" + strings.SplitN(
				ifSuffix,
				":",
				2)[1]
		} else {
			ifStr = ifSuffix
		}

		addrStr = newAddrStr
	}

	addr, err := net.ResolveTCPAddr("tcp", addrStr)
	if err != nil {
		return nil, err
	}
	return &TCPIfAddr{tcpAddr: addr, ifname: ifStr}, nil
}

func runSuccessfulDecode(cookie string, expectedType CookieType, expectedStr string, t *testing.T) {
	addrExpected, err := resolveTCPAddr(expectedStr)
	if err != nil {
		t.Fatalf("Fail to parse expected str (%s) before run the test : %s", expectedStr, err)
	}

	if addrType, err := IdentifyType(cookie); err != nil {
		t.Fatalf("Fail to identify '%s' : %s", cookie, err)
	} else if expectedType != addrType {
		t.Fatalf("Identified '%s' as type '%d' ; Expected '%d'", cookie, addrType, expectedType)
	}

	if addr, addrType, err := DecodeCookie(cookie); err != nil {
		t.Fatalf("Fail to decode '%s' : %s", cookie, err)
	} else if expectedType != addrType {
		t.Fatalf("Identified '%s' as type '%d' ; Expected '%d'", cookie, addrType, expectedType)
	} else if !addr.IP(nil).Equal(addrExpected.IP(nil)) || addr.Port(-1) != addrExpected.Port(-1) || addr.ifname != addrExpected.ifname {
		t.Fatalf("Decoded '%s' as '%s' ; Expected '%s'", cookie, addr.String(), expectedStr)
	}
}

func TestDecodeIPv4(t *testing.T) {
	runSuccessfulDecode("1677787402.36895.0000", IPv4, "10.1.1.100:8080", t)
	runSuccessfulDecode("rd5o00000000000000000000ffffc0000201o80", IPv4NDR, "192.0.2.1%5:80", t)
}

func TestDecodeIPv6(t *testing.T) {
	runSuccessfulDecode("vi20010112000000000000000000000030.20480", IPv6, "[2001:112::30]:80", t)
	runSuccessfulDecode("rd3o20010112000000000000000000000030o80", IPv6NDR, "[2001:112::30]%3:80", t)
}

type DecodeFunc func(string) (*TCPIfAddr, CookieType, error)

func runErrorDecode(cookie string, expectedType CookieType, t *testing.T) {
	if addrType, err := IdentifyType(cookie); err != nil {
		t.Fatalf("Fail to identify '%s' : %s", cookie, err)
	} else if expectedType != addrType {
		t.Fatalf("Identified '%s' as type '%d' ; Expected '%d'", cookie, addrType, expectedType)
	}

	runTestIFNeeded := func(fnc DecodeFunc, ct CookieType) {
		if expectedType != ct {
			if _, _, err := fnc(cookie); err == nil {
				t.Fatalf("Failed to obtain an error while decoding '%s' as an %v", cookie, ct)
			} else if _, ok := err.(*ErrUnexpectedFormat); !ok {
				t.Fatalf("Failed to obtain an 'ErrUnexpectedFormat' error while decoding '%s' as an %v", cookie, ct)
			}
		}
	}

	runTestIFNeeded(decodeIPv4Cookie, IPv4)
	runTestIFNeeded(decodeIPv6Cookie, IPv6)
	runTestIFNeeded(decodeIPv4NDRCookie, IPv4NDR)
	runTestIFNeeded(decodeIPv6NDRCookie, IPv6NDR)
}

func TestBadDecode(t *testing.T) {
	runErrorDecode("1677787402.36895.0000", IPv4, t)
	runErrorDecode("rd5o00000000000000000000ffffc0000201o80", IPv4NDR, t)
	runErrorDecode("vi20010112000000000000000000000030.20480", IPv6, t)
	runErrorDecode("rd3o20010112000000000000000000000030o80", IPv6NDR, t)
}
