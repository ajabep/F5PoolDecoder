package main

import (
	"encoding/binary"
	"encoding/hex"
	"errors"
	"flag"
	"fmt"
	"log"
	"net"
	"os"
	"strconv"
	"strings"
)

// CookieType defines the type of IP described by a cookie
type CookieType int

// Differents types of IP described by a cookie
// NDR means w/ No Default Route
const (
	IPv4 CookieType = iota
	IPv4NDR
	IPv6
	IPv6NDR
)

// ErrUnknownFormat is the error returned when the type of the provided cookie is not recognized
type ErrUnknownFormat struct {
	cookie string
}

func (e ErrUnknownFormat) Error() string {
	return fmt.Sprintf("Unknown cookie format: %v", e.cookie)
}

// ErrUnexpectedFormat is the error returned when we provide a wrong type of cookie string
type ErrUnexpectedFormat struct {
	expectedtype CookieType
	realtype     CookieType
	cookie       string
}

func (e *ErrUnexpectedFormat) Error() string {
	return fmt.Sprintf("Unexpected cookie format: expected %v, got %v for cookie %v", e.expectedtype, e.realtype, e.cookie)
}

// TCPIfAddr is my derivation of `net.TCPAddr` integating a string describing the used interface
type TCPIfAddr struct {
	tcpAddr *net.TCPAddr
	ifname  string
}

// TCPAddr converts a `TCPIfAddr` struct to a `net.TCPAddr`
func (t *TCPIfAddr) TCPAddr(newTCPAddr *net.TCPAddr) *net.TCPAddr {
	if newTCPAddr != nil {
		t.tcpAddr = newTCPAddr
	}
	return t.tcpAddr
}

// IP converts a `TCPIfAddr` struct to a `net.IP`
func (t *TCPIfAddr) IP(newip net.IP) net.IP {
	if newip != nil {
		t.tcpAddr.IP = newip
	}
	return t.tcpAddr.IP
}

// Port returns the port number of a `TCPIfAddr` struct
func (t *TCPIfAddr) Port(newPort int) int {
	if newPort != -1 {
		t.tcpAddr.Port = newPort
	}
	return t.tcpAddr.Port
}

func (t *TCPIfAddr) String() string {
	if t.ifname != "" {
		return fmt.Sprintf("%v%%%v:%v", t.IP(nil), t.ifname, t.Port(-1))
	}
	return fmt.Sprintf("%v:%v", t.IP(nil), t.Port(-1))
}

func decodeIPv6(ipStr string) ([]byte, error) {
	ipBytes, err := hex.DecodeString(ipStr)
	if err != nil {
		return nil, err
	}
	return ipBytes, nil
}

func decodePort(portStr string) (int, error) {
	portBuf := make([]byte, 2)
	portCookie, err := strconv.Atoi(portStr)
	if err != nil {
		return -1, err
	}
	binary.LittleEndian.PutUint16(portBuf, uint16(portCookie))
	return int(binary.BigEndian.Uint16(portBuf)), nil
}

func decodeIPv4Cookie(cookie string) (*TCPIfAddr, CookieType, error) {
	cookietype := IPv4
	addr := &TCPIfAddr{tcpAddr: &net.TCPAddr{IP: net.IPv4(0, 0, 0, 0)}}

	if t, err := IdentifyType(cookie); err != nil {
		return nil, -1, err
	} else if t != cookietype {
		return nil, -1, &ErrUnexpectedFormat{cookietype, t, cookie}
	}

	ipCookie, err := strconv.Atoi(strings.Split(cookie, ".")[0])
	if err != nil {
		return nil, -1, err
	}
	binary.LittleEndian.PutUint32(addr.IP(nil)[12:], uint32(ipCookie))

	port, err := decodePort(strings.Split(cookie, ".")[1])
	if err != nil {
		return nil, -1, err
	}
	addr.Port(port)

	return addr, cookietype, nil
}

func decodeIPv6Cookie(cookie string) (*TCPIfAddr, CookieType, error) {
	var err error
	cookietype := IPv6
	addr := &TCPIfAddr{tcpAddr: &net.TCPAddr{IP: net.IPv4(0, 0, 0, 0)}}

	if t, err := IdentifyType(cookie); err != nil {
		return nil, -1, err
	} else if t != cookietype {
		return nil, -1, &ErrUnexpectedFormat{cookietype, t, cookie}
	}

	ipStr := strings.TrimPrefix(
		strings.Split(cookie, ".")[0],
		"vi")
	portStr := strings.Split(cookie, ".")[1]
	ip, err := decodeIPv6(ipStr)
	if err != nil {
		return nil, -1, err
	}
	addr.IP(ip)

	port, err := decodePort(portStr)
	if err != nil {
		return nil, -1, err
	}
	addr.Port(port)

	return addr, cookietype, nil
}

func decodeIPv4NDRCookie(cookie string) (*TCPIfAddr, CookieType, error) {
	cookietype := IPv4NDR

	if t, err := IdentifyType(cookie); err != nil {
		return nil, -1, err
	} else if t != cookietype {
		return nil, -1, &ErrUnexpectedFormat{cookietype, t, cookie}
	}

	addr, err := decodeNDRAddr(cookie)
	return addr, cookietype, err
}

func decodeIPv6NDRCookie(cookie string) (*TCPIfAddr, CookieType, error) {
	cookietype := IPv6NDR

	if t, err := IdentifyType(cookie); err != nil {
		return nil, -1, err
	} else if t != cookietype {
		return nil, -1, &ErrUnexpectedFormat{cookietype, t, cookie}
	}

	addr, err := decodeNDRAddr(cookie)
	return addr, cookietype, err
}

func decodeNDRAddr(cookie string) (*TCPIfAddr, error) {
	var err error
	addr := &TCPIfAddr{tcpAddr: &net.TCPAddr{IP: net.IPv4(0, 0, 0, 0)}}
	addr.ifname = strings.TrimPrefix(
		strings.Split(cookie, "o")[0],
		"rd")
	ipStr := strings.Split(cookie, "o")[1]
	portStr := strings.Split(cookie, "o")[2]

	ip, err := decodeIPv6(ipStr)
	if err != nil {
		return nil, err
	}
	addr.IP(ip)

	port, err := strconv.Atoi(portStr)
	if err != nil {
		return nil, err
	}
	addr.Port(port)

	return addr, nil
}

// IdentifyType return the type of a cookie string
func IdentifyType(cookie string) (CookieType, error) {
	if len(strings.Split(cookie, ".")) == 3 {
		return IPv4, nil
	} else if strings.HasPrefix(cookie, "vi") {
		return IPv6, nil
	} else if strings.HasPrefix(cookie, "rd") {
		if strings.Contains(cookie, "o00000000000000000000ffff") {
			return IPv4NDR, nil
		}
		return IPv6NDR, nil
	}
	return -1, ErrUnknownFormat{cookie}
}

// DecodeCookie decodes the content of a cookie
func DecodeCookie(cookie string) (*TCPIfAddr, CookieType, error) {
	cookietype, err := IdentifyType(cookie)
	if err != nil {
		return nil, -1, err
	}

	switch cookietype {
	case IPv4:
		return decodeIPv4Cookie(cookie)
	case IPv6:
		return decodeIPv6Cookie(cookie)
	case IPv4NDR:
		return decodeIPv4NDRCookie(cookie)
	case IPv6NDR:
		return decodeIPv6NDRCookie(cookie)
	default:
		return nil, -1, errors.New("Not Implemented") // Fallback which should never been trigger
	}
}

func printUsage() {
	fmt.Printf("Usage: %s [OPTIONS] [--] COOKIE_VALUE\n", os.Args[0])
	flag.PrintDefaults()
}

func main() {
	log.SetFlags(0)
	flag.Usage = printUsage
	flag.Parse()
	if flag.NArg() != 1 {
		flag.Usage()
		os.Exit(2)
	}
	cookieValue := flag.Arg(0)

	fmt.Printf("Decoding %s...\n", cookieValue)
	addr, taddr, err := DecodeCookie(cookieValue)
	if err != nil {
		log.Fatal(err)
	}

	log.Print("Type: ")
	switch taddr {
	case IPv4:
		log.Println("IPv4 in default route domains")
	case IPv4NDR:
		log.Println("IPv4 in non-default route domains")
	case IPv6:
		log.Println("IPv6 in default route domains")
	case IPv6NDR:
		log.Println("IPv6 in non-default route domains")
	default:
		log.Fatalf("Unknown type (%v)", taddr)
	}
	log.Printf("IP: %v\n", addr.IP(nil))
	log.Printf("Port: %v\n", addr.Port(-1))
	if taddr == IPv4NDR || taddr == IPv6NDR {
		log.Printf("Ifname: %v\n", addr.ifname)
	}
}
